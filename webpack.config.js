const path = require('path');
const autoprefixer = require('autoprefixer');

module.exports = {
  entry: './lib/client.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve('./build'),
  },
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel', exclude: /node_modules/ },
      { test: /\.css$/, loader: 'style!css?modules!postcss' },
      { test: /\.html$/, loader: 'file?name=[name].[ext]' }
    ],
  },
  postcss: [
    autoprefixer({ browsers: ['last 2 versions'] }),
  ],
  resolve: {
    modulesDirectories: ['node_modules', 'lib']
  },
};
