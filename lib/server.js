import express from 'express';
import path from 'path';

const app = express();

app.use(express.static(path.resolve(process.cwd(), 'build')));

app.get('/', (req, res) => {
  res.send('hello!\n');
});

const port = process.env.PORT || 4000;

app.listen(port);

console.log('Server listening on port', port);
